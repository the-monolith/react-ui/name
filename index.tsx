import { component, React, Style } from 'local/react/component'

export const defaultNameStyle: Style = {
  display: 'inline-block'
}

export const Name = component
  .props<{
    style?: Style
    first?: string
    preferred?: string
    middle?: string
    last?: string
  }>({
    style: defaultNameStyle
  })
  .render(({ first, preferred, middle, last, style }) => (
    <div style={style}>
      {first} {preferred ? <span>&quot;{preferred}&quot; </span> : undefined}
      {middle} {last}
    </div>
  ))
